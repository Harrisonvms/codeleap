import * as actionTypes from '../../actions/actions'

const initialState = {
    userData: {
        name: "",
        id: 1,
    },
    posts: []
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.USER_LOGIN:
            return {
                ...state,
                userData: {
                    name: action.userName,
                    id: 1
                }
            }
        case actionTypes.USER_POSTED:
            return {
                ...state,
                posts: action.userPosts
            }    
        default:
            return state
    }
}

export default reducer