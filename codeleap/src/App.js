import React, { Component } from "react";
import { Route, Switch } from "react-router-dom";

import Layout from "./hoc/Layout/Layout";
import LoginPage from "./pages/Login/Login";
import Posts from "./pages/Posts/Posts";

class App extends Component {
  render() {
    return (
      <div>
        <Layout>
          <Switch>
            <Route path="/Posts" component={Posts} />
            <Route path="/" exact component={LoginPage} />
          </Switch>
        </Layout>
      </div>
    );
  }
}

export default App;
