import React, { Component } from "react";

import Posts from "../../components/Posts/Posts/Posts"

class PostsPage extends Component {
  render() {
    return (
      <div>
        <Posts />
      </div>
    );
  }
}

export default PostsPage;
