import React, { Component } from "react";

import Aux from "../../hoc/Aux/Aux";
import Intro from "../../components/Intro/Intro";
import Login from "../../components/Forms/Login/Login";
import Modal from "../../components/UI/Modal/Modal";

class LoginPage extends Component {
  state = {
    showLoginForm: false,
    resetLoginData: "",
  };

  loginHandler = () => {
    this.setState({ showLoginForm: true });
  };

  loginCancelHandler = () => {
    this.setState({ showLoginForm: false });
  };

  loginPermissionHandler = () => {
    this.props.history.push("/Posts");
  };

  render() {
    return (
      <Aux>
        <Modal
          show={this.state.showLoginForm}
          modalClosed={this.loginCancelHandler}
        >
          <Login loginClicked={this.loginPermissionHandler} />
        </Modal>
        <Intro show={!this.state.showLoginForm} login={this.loginHandler} />
      </Aux>
    );
  }
}

export default LoginPage;
