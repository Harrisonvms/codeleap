import React from "react";

import codeleapLogo from "../../assets/images/codeleap_logo_black.png";
import classes from "./Intro.css";

const intro = (props) => (
  <div
    className={classes.container}
    style={{
      transform: props.show ? "translateY(0)" : "translateY(-100vh)",
      opacity: props.show ? "1" : "0",
    }}
  >
    <div className={classes.content}>
      <img src={codeleapLogo} alt="Codeleap Logo" />
      <button className={classes.button} onClick={props.login}>
        Log in
      </button>
    </div>
  </div>
);

export default intro;
