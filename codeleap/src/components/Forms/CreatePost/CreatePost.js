import React, { Component } from "react";
import { connect } from "react-redux"
import axios from 'axios'

import classes from "./CreatePost.css";
import Aux from "../../../hoc/Aux/Aux";
import * as actionTypes from "../../../actions/actions"

class CreatePost extends Component {
    state = {
      postTitle: "",
      postContent: "",
      userName: "",
      posts: []
    };
      
    titleChangeHandler = (event) => {
      this.setState({ postTitle: event.target.value });
    };

    contentChangeHandler = (event) => {
      this.setState({ postContent: event.target.value });
    };

    createPostHandler = async () => {
      // const userName = this.props.usr.name
      const postData = {
        username: this.props.usr.name,
        title: this.state.postTitle,
        content: this.state.postContent
      }
      await this.postHandler(postData)
      this.clearFields()
    }

    postHandler = (payload) => {
      axios.post('https://dev.codeleap.co.uk/careers/', payload)
        .then(res => {
          this.fetchPosts()
        })
        .catch(err => {
          console.log(err)
        })
    }

    fetchPosts = () => {
      axios.get('https://dev.codeleap.co.uk/careers/')
      .then(res => {
        this.props.onUserPost(res.data.results)
      })
      .catch(err => {
        console.log(err)
      })
    }

    clearFields = () => {
      this.setState({ postTitle: "" });
      this.setState({ postContent: "" });
    };
  
    render() {      
      return (
        <Aux>
          <div className={classes.container}>
            <div className={classes.header}>
              <span className={classes.title}>What’s on your mind {this.props.usr.name}? </span>
            </div>
            <div className={classes.form}>
              <span className={classes.label}> Title </span>
              <div className={classes.fieldBody}>
                <input
                  className={classes.field}
                  type="text"
                  value={this.state.postTitle}
                  onChange={this.titleChangeHandler}
                  placeholder="Hello World"
                />
              </div>
              <br/>
              <span className={classes.label}> Content </span>
              <div className={classes.fieldBody}>
                <div className={classes.textAreaBody}>
                    <textarea className={classes.textArea} value={this.state.postContent} onChange={this.contentChangeHandler} placeholder="Content here"/>
                </div>
              </div>
            </div>
            <div className={classes.footer}>
              {this.state.postTitle && this.state.postContent ? (
                <button
                  className={classes.button}
                  onClick={this.createPostHandler}
                >
                  CREATE
                </button>
              ) : (
                <button className={classes.buttonDisabled}>CREATE</button>
              )}
            </div>
          </div>
        </Aux>
      );
    }
}

const mapStateToProps =  state => {
  return {
    usr: state.userData,
    usrPost: state.posts
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onUserPost: (usrpost) => dispatch({type: actionTypes.USER_POSTED, userPosts: usrpost})
  }
}

export default connect(mapStateToProps , mapDispatchToProps)(CreatePost)

