import React, { Component } from "react";
import { connect } from "react-redux"

import classes from "./Login.css";
import Aux from "../../../hoc/Aux/Aux";
import * as actionTypes from "../../../actions/actions"

class Login extends Component {
  state = {
    userName: "",
  };

  changeHandler = (event) => {
    this.setState({ userName: event.target.value });
    this.props.onUserLogged(event.target.value)
  };

  render() {
    return (
      <Aux>
        <div className={classes.container}>
          <div className={classes.header}>
            <span className={classes.title}>Welcome to CodeLeap network!</span>
          </div>
          <div className={classes.form}>
            <span className={classes.label}> Please enter your username </span>
            <div className={classes.fieldBody}>
              <input
                className={classes.field}
                type="text"
                value={this.state.userName}
                onChange={this.changeHandler}
              />
            </div>
          </div>
          <div className={classes.footer}>
            {this.state.userName ? (
              <button
                className={classes.button}
                onClick={this.props.loginClicked}
              >
                ENTER
              </button>
            ) : (
              <button className={classes.buttonDisabled}>ENTER</button>
            )}
          </div>
        </div>
      </Aux>
    );
  }
}

const mapStateToProps =  state => {
  return {
    usr: state.userData
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onUserLogged: (usrname) => dispatch({type: actionTypes.USER_LOGIN, userName: usrname})
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(Login);
