import React, { Component } from "react";
import axios from 'axios'

import classes from "./Post.css";
import Aux from "../../../hoc/Aux/Aux";
import TrashIcon from "../../../assets/images/Trash.png";
import EditIcon from "../../../assets/images/Edit.png";
import Modal from "../../../components/UI/Modal/Modal";

class Post extends Component {
  state = {
    postTitle: "",
    postContent: "",
    showEditForm: false,
    isEdit: false
  };

  componentDidMount() {
    this.fetchPosts()
  }

  deleteHandler = () => {
    this.setState({ showEditForm: true });
  };

  deleteMethod = () => {
     axios.get(`https://dev.codeleap.co.uk/careers/${this.props.postId}/`, {})
     .then(res => {
       console.log(res)
       this.fetchPosts()
       this.setState({ showEditForm: false });
     })
     .catch(err => {
       console.log(err)
     })
  }

  fetchPosts = () => {
    axios.get('https://dev.codeleap.co.uk/careers/')
    .then(res => {

    })
    .catch(err => {
      console.log(err)
    })
  } 

  editCancelHandler = () => {
    this.setState({ showEditForm: false });
  };

  titleChangeHandler = (event) => {
    this.setState({ postTitle: event.target.value });
  };

  contentChangeHandler = (event) => {
      this.setState({ postContent: event.target.value });
  };

  render() {
    return (
      <Aux>
        <div className={classes.container}>
          <Modal
            show={this.state.showEditForm}
            modalClosed={this.editCancelHandler}
          >
            <div className={classes.modalContent}>
              <span> Are you sure you want to delete this item? </span>
              <div className={classes.modalButtons}>
                <button onClick={this.deleteMethod}> ok </button>
              </div>
            </div>
          </Modal>
          <div className={classes.header}>
            <div>
              <span className={classes.title}> {this.props.postTitle} </span>
            </div>
            <div className={classes.icons}>
              <img src={TrashIcon} alt="delete" onClick={this.deleteHandler}/>
              <img src={EditIcon} alt="edit" />
            </div>
          </div>
          <div className={classes.content}>
            <div className={classes.contentHeader}>
              <span className={classes.subtitle}> @{this.props.userName} </span>
              <span className={classes.subtitle}> {this.props.postDate} </span>
            </div>
            <div className={classes.contentText}>
              <span className={classes.textDecoration}> {this.props.postContent}  </span>
            </div>
          </div>
        </div>
      </Aux>
    );
  }
}

export default Post;
