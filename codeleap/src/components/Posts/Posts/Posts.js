import React , { Component } from 'react'
import { connect } from "react-redux"
import axios from 'axios'

import classes from "./Posts.css"
import CreatePost from "../../Forms/CreatePost/CreatePost"
import Post from "../../Posts/Post/Post"
import * as actionTypes from "../../../actions/actions"

class Posts extends Component {  
    componentDidMount() {
        this.fetchPosts()
    }

    fetchPosts = () => {
        axios.get('https://dev.codeleap.co.uk/careers/')
        .then(res => {
          this.props.onUserPost(res.data.results)
        })
        .catch(err => {
          console.log(err)
        })
    }
    
    render() {     
      return (
        <div className={classes.container}>
            <div className={classes.body}>
                <div className={classes.header}>
                    <span className={classes.headerTitle}> CodeLeap Network </span>
                </div>
                <div className={classes.formContainer}>
                    <div className={classes.form}>
                        <CreatePost />
                    </div>
                    <div className={classes.posts}>
                        {this.props.usrPost.map((post, index) => (
                            <Post 
                                userName={post.username} 
                                postTitle={post.title} 
                                postContent={post.content}
                                postDate={post.created_datetime}
                                postId={post.id}
                                key={index}
                            />
                        ))}
                    </div>
                </div>
            </div>
        </div>
      );
    }
}

const mapStateToProps =  state => {
    return {
      usrPost: state.posts
    }
}

const mapDispatchToProps = dispatch => {
    return {
      onUserPost: (usrpost) => dispatch({type: actionTypes.USER_POSTED, userPosts: usrpost})
    }
}

export default connect(mapStateToProps , mapDispatchToProps)(Posts)